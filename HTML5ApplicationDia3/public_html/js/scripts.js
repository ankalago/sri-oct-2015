/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global myData, key, setHeader */

//"use strict";

$(document).ready(function () {
    //alert("ok");

    var div1 = $("#div1");
    var div2 = $("#div2");

    var redClass = $(".red");
    var blueClass = $(".blue");

    $("#button1").click(function () {

        var div1 = $("#div1");
        var div2 = $("#div2");

        var redClass = $(".red");
        var blueClass = $(".blue");

        var colorDiv1 = div1.css("background");
        var colorDiv2 = div2.css("background");

        div1.css("background", colorDiv2);
        div2.css("background", colorDiv1);

    });

    $("#button2").click(function () {
        //alert("div2: " + div2.css("background"))

        redClass.toggleClass("blue");
        blueClass.toggleClass("red");
    });

    $("#append").click(function () {

        $("#contentAppend").append($("#tableAppend"));

    });

    $("#replace").click(function () {

        $("h1").replaceWith("<h2>Elemento H2</h2>");

    });

    $("#empty").click(function () {

        $("#emptyChild").empty(); //vacia el contenido hijo

    });

    $("#attr").click(function () {

        $("#linkAttr").attr("href", "cambio.html");

    });

    $("#css").click(function () {

        $("#cambioCSS").css({
            "background": "blue",
            "color": "black",
            "width" : "50%"
        });

    });
    
    
    $("#cambioCSS")
        .mouseover(function(){
            $(this).css({
                "background": "blue",
                "color": "black",
                "width" : "50%"
            });
        }).mouseout(function(){
            $(this).css({
                "background": "red",
                "color": "white",
                "width" : "98%"
            });
        });
        
        
    $("#bind").bind("click",function () {

        $("#bindOne").css({
            "background": "violet",
            "width" : "50%"
        });

    });
    
    $("#one").one("click",function () {
        
        alert("evento unico e irrepetible");

    });
    
    $("#slideToggle").click(function () {
        
        $("#slide").slideToggle(200);

    });
    
    $("#fadeIn").click(function () {
        
        $("#fade").fadeIn(200);

    });
    
    $("#fadeOut").click(function () {
        
        $("#fade").fadeOut(200); //fadeToggle

    });
    
    $("#fadeTo").click(function () {
        
        $("#fade").fadeTo("slow",0.5); //fadeToggle

    });
    
    $("#animation").click(function () {
        
        $("#animate").animate({
            width : "50%",
            //"opacity" : 0.5,
            backgroundColor: "blue" 
        },1000);

    });
    
    /*$("#load").load({
        url: "https://www.google.com.ec",
       headers: { 'Access-Control-Allow-Origin': '*' },
       crossDomain: true    
    });
    */
   
    $( "#load" ).load( "http://localhost:8383/HTML5Application/index.html #window" );

    formatTable();
});

function intercambiarColores() {
    var div1 = $("#div1");
    var div2 = $("#div2");

    div1.removeClass("blue");
    div1.addClass("red");

    div2.removeClass("red");
    div2.addClass("blue");
}

function formatTable() {
    $("table tr").each(function (i) {
        //alert("fila" + i)

        if (i % 2) {
            $(this).addClass("odd");
        }
    });
}
